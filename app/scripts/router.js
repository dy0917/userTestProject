Gladeyetestproject.Router.map(function() {
    // Add your routes here

    this.resource("init", {path: '/'}, function()
    {
        this.resource("init", {path: '/'});
        this.resource("users", {path: '/users'});
        this.resource("userNew", {path: '/user/new'});
        this.resource("user", {path: '/user/:id'});
        this.resource("userEdit", {path: '/user/edit/:id'});
        this.resource("login", {path: '/login'});
    }

    );
});
