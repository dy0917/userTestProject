//Gladeyetestproject.ApplicationAdapter = DS.FixtureAdapter;


Gladeyetestproject.store = DS.Store.create({
  
    adapter: DS.RESTAdapter.create({
        bulkCommit: false,
        url: getRestAPIURL()

    })
});

DS.RESTAdapter.reopen({
  host: getRestAPIURL()
});