/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Gladeyetestproject.UsersController = Ember.Controller.extend({
    content: "",
    needs: ["application", "popupErrorBox"],
    isAuthorized: false,
    actions: {
        setContent: function(content) {
            for (var i = 0; i < content.length; i++)
            {
                this.get("content").pushObject(content[i]);
            }
            //    this.set("content", content);
        },
        clickNewUser: function() {
            var user = this.store.createRecord('user', {
            });
            this.get("content").insertAt(0, user);
        },
        setIsAuthorized: function()
        {
            var applicationController = this.get('controllers.application');
            var loginUser = applicationController.get("loginedUser");
            if (loginUser.get("group") == "2")
            {
                this.set("isAuthorized", false);
            }
            else
            {
                this.set("isAuthorized", true);
            }
        }
    },
});
