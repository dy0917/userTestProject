/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Gladeyetestproject.UserController = Ember.Controller.extend({
    isEdit: false,
    isDisplayErrorPupop: false,
    errorlist: [],
    needs: ["application", "popupErrorBox", "popupDeleteBox"],
    isAuthorized: false,
    init: function()
    {
        var applicationController = this.get('controllers.application');
        var loginUser = applicationController.get("loginedUser");

        if (loginUser.get("group") == "2")
        {

            this.set("isAuthorized", false);
        }
        else
        {
            this.set("isAuthorized", true);
        }
    },
    actions: {
        switchIsEdit: function() {
            this.set("isEdit", !this.get("isEdit"));
        },
        save: function()
        {
            var that = this;
            if (this.isInputValidate())
            {
                this.get("model").save().then(function(response) {
                    that.send("switchIsEdit");
                    that.set("errorlist", []);
                    //server responded with {"myModel:{"id":1,"name":"someName","value":"someValue"}"}
                }, function(response) {
                    that.get("model").destroyRecord();
                    //if failure
                    //server responded with {"error":"some custom error message"}
                    //BUT HOW TO CATCH THIS AND POSSIBLY REMOVE THE MODEL FROM THE STORE
                    if (response.responseText === 'email_exist') {
                        var applicationController = that.get('controllers.application');
                        applicationController.send("switchisDisplayErrorPupop");
                        var popupErrorBoxController = that.get('controllers.popupErrorBox');
                        that.get("errorlist").push("email exist");
                        popupErrorBoxController.set("content", that.get("errorlist"));
                    }
                    that.set("errorlist", []);
                });
            }
            else
            {
                var applicationController = this.get('controllers.application');
                applicationController.send("switchisDisplayErrorPupop");
                var popupErrorBoxController = this.get('controllers.popupErrorBox');
                popupErrorBoxController.set("content", this.get("errorlist"));
                this.set("errorlist", []);
            }
        },
        switchActive: function()
        {
            if (this.get("model").get("active") === true)

            {
                this.set("active", false);
                this.get("model").set("active", false);
            }
            else {
                this.set("active", true);
                this.get("model").set("active", true);
            }
        }
        ,
        delete: function() {
            var applicationController = this.get('controllers.application');
            applicationController.send("switchisDeletePopup");
            var popupDeleteBoxController = this.get('controllers.popupDeleteBox');
            popupDeleteBoxController.set("user", this.get("model"));

        },
        cancel: function() {
            this.send("switchIsEdit");
        }
    },
    isInputValidate: function() {
        var isInputValidate = true;
        var model = this.get("model");
        if (model.get("first_name") == "" || model.get("first_name") == null)
        {
            this.get("errorlist").push("First name cannot be null or empty");
            isInputValidate = false;
        }
        if (model.get("last_name") == "" || model.get("last_name") == null)
        {
            this.get("errorlist").push("Last name cannot be null or empty");
            isInputValidate = false;
        }

        if (/(.+)@(.+){2,}\.(.+){2,}/.test(model.get("email"))) {
            // valid email
        } else {
            this.get("errorlist").push("please give a correct email");
            isInputValidate = false;
        }

        return isInputValidate;
    }

});
