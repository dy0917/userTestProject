/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Gladeyetestproject.PopupDeleteBoxController = Ember.Controller.extend({
    user: "",
    actions: {
        delete: function()
        {
            this.get("user").deleteRecord();
            this.get("user").save();
            this.send("switchisDeletePopup");
        }
    }
});
