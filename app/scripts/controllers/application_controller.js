/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Gladeyetestproject.ApplicationController = Ember.Controller.extend({
    isDisplayErrorPupop: false,
    isloginclick: false,
    isDeletePopup: false,
    loginedUser: "",
    init: function() {
        this.set("isloginclick", true);
    },
    actions: {
        switchisDisplayErrorPupop: function()
        {
            this.set("isDisplayErrorPupop", !this.get("isDisplayErrorPupop"));
        },
        switchisLoginPopup: function()
        {
            this.set("isloginclick", !this.get("isloginclick"));
        },
        switchisDeletePopup: function()
        {
            this.set("isDeletePopup", !this.get("isDeletePopup"));
        }
    }

});
