/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


Gladeyetestproject.RegisterformController = Ember.Controller.extend({
    //controller attributes
    needs: ["application"],
    isProcessing: false,
    reply_message: "",
    //form attributes
    firstName: "",
    lastName: "",
    password: "",
    active: true,
    registeremail: "",
    registerpassword1: "",
    registerpassword2: "",
    //form checking variables
    isCorrectEmail: true,
    isCorrectFirstName: true,
    isCorrectLastName: true,
    isCorrectPassword: true,
    isActive: true,
    // error message
    passwordErrorMessage: "",
    actions: {
        closewindow: function()
        {
            var applicationController = this.get('controllers.application');
            this.set("username", "");
            this.set("password", "");
            applicationController.send("switchLogin");
        },
        setProcessingStatus: function(btn_id, bool)
        {

            this.set("btnlogin", document.getElementById(btn_id));
            var lbtnlogin = Ladda.create(this.get("btnlogin"));
            if (bool) {
                lbtnlogin.start();
            } else {
                lbtnlogin.stop();
            }

        },
        switchActive: function()
        {


            this.set("isActive", !this.get("isActive"));
        },
        registerClick: function()
        {

            this.set("reply_message", "");
            var that = this;
            this.isFirstName();
            this.isLastName();
            this.checkregisterPassword();
            this.checkemail();

            if (this.isCorrectFirstName && this.isCorrectLastName && this.isCorrectEmail && this.isCorrectPassword)
            {
                var selectGroup = document.getElementById("selectGroup");
                var user = this.store.createRecord('user', {
                    "first_name": this.get("firstName"),
                    "last_name": this.get("lastName"),
                    "email": this.get("registeremail"),
                    "password": this.get("registerpassword1"),
                    "group": selectGroup.value,
                    "active": this.get("active")
                });

                user.save().then(function(response) {
                    that.set("reply_message", "Your account is created, please login with your details");
                   
                    //server responded with {"myModel:{"id":1,"name":"someName","value":"someValue"}"}
                }, function(response) {

                    //if failure
                    //server responded with {"error":"some custom error message"}
                    //BUT HOW TO CATCH THIS AND POSSIBLY REMOVE THE MODEL FROM THE STORE
                    if (response.responseText === 'email_exist') {
                        that.set("reply_message", "This email has been registerd.");
                    }


                });


            }

        }, setActive: function()
        {

        },
        setGroup: function(group) {
            this.set("group", group);
            console.log(this.get("group"));
        }
    }
    ,
    checkemail: function()
    {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var isCorrectEmail = false;
        isCorrectEmail = re.test(this.get("registeremail"));
        this.set("isCorrectEmail", isCorrectEmail);
        return isCorrectEmail;
    }
    ,
    checkregisterPassword: function()
    {
        var isCorrect = true;
        this.set("isCorrectPassword", isCorrect);
        this.set("passwordErrorMessage", "");
        var re = /(?=(.*[0-9]){2,})(?=(.*[A-Za-z]){1,})([A-Z0-9a-z]){7,}\w+/;
        if (!re.test(this.get("registerpassword1")))
        {
            this.set("passwordErrorMessage", "Password must be 8 characters, contain 2 numbers and one Character.");
            isCorrect = false;
            this.set("isCorrectPassword", isCorrect);
        }
        else if (this.get("registerpassword1") !== this.get("registerpassword2")) {
            this.set("passwordErrorMessage", "Password must match.");
            isCorrect = false;
            this.set("isCorrectPassword", isCorrect);
        }
        return isCorrect;
    }
    ,
    isFirstName: function()
    {
        this.set("isCorrectFirstName", true);
        if (!this.get("firstName")) {

            this.set("isCorrectFirstName", false);
        }
        return this.get("isCorrectFirstName");
    }
    ,
    isLastName: function()
    {
        this.set("isCorrectLastName", true);
        if (!this.get("lastName")) {

            this.set("isCorrectLastName", false);
        }
        return this.get("isCorrectLastName");
    }

});
