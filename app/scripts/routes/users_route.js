Gladeyetestproject.UsersRoute = Ember.Route.extend({
    // admittedly, this should be in IndexRoute and not in the
    // top level ApplicationRoute; we're in transition... :-)

    setupController: function(controller, arr) {
        var applicationController = this.controllerFor("application");

        var user = applicationController.get("loginedUser");
        if (!user)
        {
            this.transitionTo('init');
            applicationController.send("switchisLoginPopup");
        }
        else {
            controller.send("setIsAuthorized");
            var users = this.store.find('user');
        }



//        var users = this.store.filter('user', function(user) {
//            return user.get('id') !== null;
//        });
        controller.set('content', users);

    },
    model: function() {

//        var users = this.store.find('user');
//
//        return users;
    },
    renderTemplate: function() {
        this.render('users');
    }
});
